from flask import Blueprint, render_template
from flask_login import current_user

from helpers.auth import is_admin_logged

bp = Blueprint('items', __name__, url_prefix='/items')

@bp.get('/new_item')
def load_new_item_page():
    return render_template('/new_item.html', is_admin_logged=is_admin_logged(),
                           is_authenticated=current_user.is_authenticated)