from flask import Blueprint, request, flash, redirect, url_for, render_template
from flask_login import current_user

from database.database import get_db
from helpers.auth import is_admin_logged

bp = Blueprint('categories', __name__, url_prefix='/category')

@bp.get('/delete')
def delete_category():
    category_id = request.args.get('id')
    db = get_db()
    try:
        db.execute(
            "DELETE FROM kategorie WHERE id_kategorie = ?",
            (category_id)
        )
        db.commit()
        flash('Záznam byl úspěšně smazán.', 'alert success')
    except db.Error as e:
        flash('Není možno smazat záznam.', 'error')
        print(e)
    return redirect(url_for('intro'))

@bp.get('/category')
def show_category():
    category_id = request.args.get('id')
    produkty = []
    info = None
    db = get_db()
    try:
        produkty = db.execute(
            "SELECT id_produktu, produkty.nazev, vyrobce FROM produkty JOIN kategorie USING(id_kategorie) "
            "WHERE id_kategorie = ?",
            (category_id, )
        ).fetchall()
        category_info = db.execute(
            "SELECT nazev, popis, barva FROM kategorie WHERE id_kategorie = ?",
            (category_id, )
        ).fetchone()
    except db.Error as e:
        flash('Není možno načíst databázi.', 'error')
        print(e)
    return render_template('categories/categories.html', category_id=category_id, products = produkty,
                           category_info=category_info, is_admin_logged=is_admin_logged(),
                           is_authenticated=current_user.is_authenticated)

@bp.get('/newcategory')
def new_category():
    return render_template('categories/newcategory.html', data = {}, is_admin_logged=is_admin_logged(),
                           is_authenticated=current_user.is_authenticated)

@bp.post('/newcategory')
def add_category():
    db = get_db()
    data = request.form
    if (data['popis'] == ''): # pokud je bez popisu
        db.execute(
            "INSERT INTO kategorie (nazev, popis, barva) VALUES (?, ?, ?)",
            (data['nazev'], '', data['barva'])
        ).lastrowid  # přidá další id v řadě
    else: # s popisem
        db.execute(
            "INSERT INTO kategorie (nazev, popis, barva) VALUES (?, ?, ?)",
            (data['nazev'], data['popis'], data['barva'])
        ).lastrowid  # přidá další id v řadě
    db.commit()
    return redirect(url_for('intro', is_authenticated=current_user.is_authenticated, is_admin_logged=is_admin_logged()))
