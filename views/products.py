from flask import Blueprint, request, flash, redirect, url_for, render_template
from flask_login import current_user

from database.database import get_db
from helpers.auth import is_admin_logged

bp = Blueprint('products', __name__, url_prefix='/products')

@bp.get('/delete_product')
def delete_product():
    product_id = request.args.get('id')
    kategorie = 0
    db = get_db()
    try:
        kategorie = db.execute("SELECT id_kategorie FROM produkty WHERE id_produktu = ?", (product_id, ) ).fetchone()[0]
        db.execute(
            "DELETE FROM produkty WHERE id_produktu = ?",
            (product_id, )
        )
        db.commit()
        flash('Záznam byl úspěšně smazán.', 'alert success')
    except db.Error as e:
        flash('Není možno smazat záznam.', 'error')
        print(e)
    return redirect(url_for('categories.show_category', id=kategorie))

@bp.get('/product')
def show_prices():
    product_id = request.args.get('id')
    ceny = []
    product_info = None
    db = get_db()
    try:
        ceny1 = db.execute(
            "SELECT id_produktu, strftime('%Y-%m-%d', datetime(datum)) datum, cena, zdroj FROM ceny WHERE "
            "id_produktu = ? AND id_uzivatele IS NULL ", # záznamy přidané nepřihlášenými uživateli
            (product_id, )
        ).fetchall()
        ceny2 = db.execute(
            "SELECT id_produktu, strftime('%Y-%m-%d', datetime(datum)) datum, cena, username, zdroj FROM ceny "
            "JOIN uzivatele USING(id_uzivatele) WHERE id_produktu = ? "
            "AND id_uzivatele IS NOT NULL", # záznamy přidané administrátory
            (product_id, )
        ).fetchall()
        ceny = ceny1 + ceny2
        product_info = db.execute(
            "SELECT nazev, vyrobce FROM produkty WHERE id_produktu = ?",
            (product_id, )
        ).fetchone()
        category_info = db.execute(
            "SELECT barva FROM kategorie JOIN produkty USING(id_kategorie) WHERE id_produktu = ?",
            (product_id, )
        ).fetchone()
    except db.Error as e:
        flash('Není možno načíst databázi.', 'error')
        print(e)
    return render_template('products/products.html', prices = ceny, category_info=category_info,
                           product_info=product_info, is_admin_logged=is_admin_logged(),
                           is_authenticated=current_user.is_authenticated)

@bp.get('/delete_price')
def delete_price():
    product_id = request.args.get('id')
    date = request.args.get('date')
    db = get_db()
    try:
        db.execute(
            "DELETE FROM ceny WHERE id_produktu = ? AND datum = ?",
            (product_id, date)
        )
        db.commit()
        flash('Záznam byl úspěšně smazán.', 'alert success')
    except db.Error as e:
        flash('Není možno smazat záznam.', 'error')
        print(e)
    return redirect(url_for('products.show_prices', id=product_id))

@bp.get('/newproduct')
def new_product():
    categories = []
    db = get_db()
    categories = db.execute(
        "SELECT id_kategorie, nazev FROM kategorie"
    ).fetchall()
    return render_template('products/newproduct.html', data = {}, categories=categories,
                           is_admin_logged=is_admin_logged(), is_authenticated=current_user.is_authenticated)

@bp.post('/newproduct')
def add_product():
    db = get_db()
    data = request.form
    category_id = int(data['kategorie'][0])
    try:
        db.execute(
            "INSERT INTO produkty (nazev, vyrobce, id_kategorie) VALUES (?, ?, ?)",
            (data['nazev'], data['vyrobce'], category_id, )
        ).lastrowid  # přidá další id v řadě
        db.commit()
        flash('Záznam byl úspěšně vložen.', 'alert success')
    except db.Error as e:
        flash('Není možno přidat záznam - pravděpodobně jste zadali špatný název kategorie.', 'error')
        print(e)
    return redirect(url_for('categories.show_category', id=category_id, is_authenticated=current_user.is_authenticated,
                            is_admin_logged=is_admin_logged()))

@bp.get('/newpriceadm')
def new_price_admin():
    products = []
    db = get_db()
    products = db.execute(
        "SELECT id_produktu, nazev FROM produkty"
    ).fetchall()
    return render_template('products/newprice_admin.html', data = {}, products=products,
                           is_admin_logged=is_admin_logged(), is_authenticated=current_user.is_authenticated)

@bp.get('/newprice')
def new_price_anonymous():
    products = []
    db = get_db()
    products = db.execute(
        "SELECT id_produktu, nazev FROM produkty"
    ).fetchall()
    return render_template('products/newprice_anonymous.html', data = {}, products=products,
                           is_admin_logged=is_admin_logged(), is_authenticated=current_user.is_authenticated)

@bp.post('/newpriceadm')
def add_price_admin():
    db = get_db()
    data = request.form
    product_id = data['produkt']
    try:
        user_id = current_user.get_id()
        db.execute(
            "INSERT INTO ceny (datum, id_produktu, cena, zdroj, id_uzivatele) VALUES (DATE(?), ?, ?, ?, ?)",
            (str(data['datum']), product_id, data['castka'], data['zdroj'], user_id)
        ).lastrowid  # přidá další id v řadě
        db.commit()
        flash('Záznam byl úspěšně vložen.', 'alert success')
    except db.Error as e:
        flash('Není možno přidat záznam - pravděpodobně jste zadali špatný název produktu.', 'error')
        print(e)
    return redirect(url_for('products.show_prices', id=product_id, is_authenticated=current_user.is_authenticated,
                            is_admin_logged=is_admin_logged()))


@bp.post('/newprice')
def add_price_anonymous():
    db = get_db()
    data = request.form
    product_id = data['produkt']
    try:
        db.execute(
            "INSERT INTO ceny (datum, id_produktu, cena, zdroj) VALUES (?, ?, ?, ?)",
            (str(data['datum']), product_id, data['castka'], data['zdroj'])
        ).lastrowid  # přidá další id v řadě
        db.commit()
        flash('Záznam byl úspěšně vložen.', 'alert success')
    except db.Error as e:
        flash('Není možno přidat záznam - pravděpodobně jste zadali špatný název produktu.', 'error')
        print(e)
    return redirect(url_for('products.show_prices', id=product_id, is_authenticated=current_user.is_authenticated,
                            is_admin_logged=is_admin_logged()))
