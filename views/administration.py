from flask import Blueprint, render_template, flash, request, redirect, url_for
from flask_login import current_user

from database.database import get_db
from helpers.auth import is_admin_logged

bp = Blueprint('administration', __name__, url_prefix='/administration')

@bp.get('/users')
def load_admin_page():
    db = get_db()
    uzivatele_role = []
    try:
        uzivatele_role = db.execute("SELECT id_uzivatele,username, nazev FROM uzivatele NATURAL JOIN role_uzivatelu "
                                    "NATURAL JOIN role").fetchall()
    except db.Error as e:
        flash('Nelze načíst data z databáze.', 'error')
    return render_template('/administration.html', users_roles = uzivatele_role, is_admin_logged=is_admin_logged(),
                           is_authenticated=current_user.is_authenticated)

@bp.get('/delete')
def delete_user():
    user_id = request.args.get('id')
    db = get_db()
    try:
        db.execute("DELETE FROM role_uzivatelu WHERE id_uzivatele = ?", (user_id, ))
        db.execute("DELETE FROM uzivatele WHERE id_uzivatele = ?", (user_id, ))
        db.commit()
        flash('Uživatel byl smazán.', 'success')
    except db.Error as e:
        flash('Chyba při mazání.', 'error')
        print(e)
    return redirect(url_for('administration.load_admin_page'))

@bp.get('/grant')
def grant_permissions():
    user_id = request.args.get('id')
    db = get_db()
    try:
        db.execute("UPDATE role_uzivatelu SET id_role = (SELECT id_role FROM role WHERE nazev = 'administrator') "
                   "WHERE id_uzivatele = ?", (user_id, ))
        db.commit()
        flash('Uživateli byla přidělena práva administrátora.', 'success')
    except db.Error as e:
        flash('Chyba při práci s db.', 'error')
        print(e)
    return redirect(url_for('administration.load_admin_page'))
