from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask_login import LoginManager, login_user, current_user, logout_user

from database.database import get_db
from helpers.User import User
from helpers.auth import password_hash, is_admin_logged

bp = Blueprint('auth', __name__, url_prefix='/auth')

login_manager = LoginManager()

@login_manager.user_loader
def load_user(id_uzivatele):
    db = get_db()
    try:
        user_data = db.execute(
            "SELECT * FROM uzivatele WHERE id_uzivatele = ?",
            (id_uzivatele, )
        ).fetchone()
        if user_data is None:
            return None
        else:
            return User(user_data['id_uzivatele'], user_data['username'])
    except db.Error as e:
        flash('Omlouváme se, nastal problém s databází.', 'error')
        print('DB Error: ' + str(e))
        return None


@bp.get('/login')
def login_form():
    after_registration = request.args.get('after_registration')
    return render_template('auth/login.html', data={}, after_registration=after_registration,
                           is_authenticated=current_user.is_authenticated, is_admin_logged=is_admin_logged())

@bp.post('/login')
def login_action():
    db = get_db()
    data = request.form
    try:
        user_data = db.execute(
            "SELECT * FROM uzivatele WHERE username = ? AND password = ?",
            (data['username'], password_hash(data['password'])) #password_hash(data['password'])
        ).fetchone()
        if user_data is None:
            flash('Zadali jste nesprávné přihlašovací údaje.', 'error')
            return render_template('auth/login.html', data=data)

        user = User(user_data['id_uzivatele'], user_data['username'])
        login_user(user)
        return redirect(url_for('intro', after_login = True))
    except db.Error as e:
        flash('Omlouváme se, nastal problém s databází.', 'error')
        print('DB Error: ' + str(e))
        return render_template('auth/login.html', data=data, is_admin_logged=is_admin_logged(),
                               is_authenticated=current_user.is_authenticated)

@bp.get('/logout')
def logout_action():
    logout_user()
    return redirect(url_for('intro', after_logout = True, is_admin_logged=is_admin_logged(),
                            is_authenticated=current_user.is_authenticated))

@bp.get('/registration')
def registration():
    return render_template('auth/registration.html', data={})

@bp.post('/registration')
def register():
    db = get_db()
    data = request.form
    db.execute(
        "INSERT INTO uzivatele (username, password) VALUES (?, ?)",
        (data['username'], password_hash(data['password']))
    ).lastrowid  # přidá další id v řadě
    user_id = db.execute(
        "SELECT id_uzivatele FROM uzivatele WHERE username = ? and password = ?",
        (data['username'], password_hash(data['password']))
    ).fetchone()
    db.execute(
        "INSERT INTO role_uzivatelu(id_uzivatele, id_role) VALUES(?, 0)",
        (user_id)
    )
    db.commit()
    return redirect(url_for('auth.login_form', after_registration=True, is_authenticated=current_user.is_authenticated,
                            is_admin_logged=is_admin_logged()))
