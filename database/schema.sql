DROP TABLE IF EXISTS kategorie;
DROP TABLE IF EXISTS produkty;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS uzivatele;
DROP TABLE IF EXISTS role_uzivatelu;
DROP TABLE IF EXISTS ceny;

CREATE TABLE kategorie (
    id_kategorie INTEGER PRIMARY KEY AUTOINCREMENT,
    nazev TEXT NOT NULL,
    popis TEXT,
    barva  TEXT NOT NULL
);

CREATE TABLE produkty (
    id_produktu INTEGER PRIMARY KEY AUTOINCREMENT,
    nazev TEXT NOT NULL,
    vyrobce TEXT NOT NULL,
    id_kategorie NUMBER NOT NULL,
    FOREIGN KEY (id_kategorie) REFERENCES kategorie(id_kategorie)
);

CREATE TABLE role (
    id_role INTEGER PRIMARY KEY AUTOINCREMENT,
    nazev TEXT NOT NULL
);

CREATE TABLE uzivatele (
    id_uzivatele INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    password TEXT NOT NULL
);

CREATE TABLE role_uzivatelu (
    id_uzivatele NUMBER NOT NULL,
    id_role NUMBER NOT NULL,
    PRIMARY KEY (id_role, id_uzivatele),
    FOREIGN KEY (id_uzivatele) REFERENCES uzivatele(id_uzivatele),
    FOREIGN KEY (id_role) REFERENCES role(id_role)
);

CREATE TABLE ceny (
    datum TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    id_produktu NUMBER NOT NULL,
    cena NUMBER NOT NULL,
    zdroj TEXT,
    id_uzivatele NUMBER,
    FOREIGN KEY (id_produktu) REFERENCES produkty(id_produktu),
    PRIMARY KEY (id_produktu, datum),
    FOREIGN KEY (id_uzivatele) REFERENCES uzivatele(id_uzivatele)
);

