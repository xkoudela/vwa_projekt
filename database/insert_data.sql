-- role
INSERT INTO ROLE (id_role, nazev) VALUES (0, 'registrovany');
INSERT INTO ROLE (id_role, nazev) VALUES (1, 'administrator');

--admin user
INSERT INTO uzivatele (id_uzivatele, username, password) VALUES (1,'ja','fa36c7c07d2f16a1a20b4bd4db42b88434a476df9e1d574f3f91569322c87712');
INSERT INTO role_uzivatelu (id_uzivatele, id_role) VALUES (1,1);

-- kategorie
INSERT INTO KATEGORIE (id_kategorie, nazev, popis, barva) VALUES (1, 'prvni kategorie', 'ne', '#f28e1c');
INSERT INTO KATEGORIE (id_kategorie, nazev, popis, barva) VALUES (2, 'druhá kategorie', 'tu', '#f28e1c');
INSERT INTO KATEGORIE (id_kategorie, nazev, popis, barva) VALUES (3, 'třetí kategorie', 'ším', '#f28e1c');

-- produkty
INSERT INTO PRODUKTY (id_produktu, nazev, vyrobce, id_kategorie) VALUES (1, 'První', 'vyrobce1', 1);
INSERT INTO PRODUKTY (id_produktu, nazev, vyrobce, id_kategorie) VALUES (2, 'Druhý', 'vyrobce2', 1);
INSERT INTO PRODUKTY (id_produktu, nazev, vyrobce, id_kategorie) VALUES (3, 'Třetí', 'vyrobce3', 2);
INSERT INTO PRODUKTY (id_produktu, nazev, vyrobce, id_kategorie) VALUES (4, 'Čtvrtý', 'vyrobce4', 2);
INSERT INTO PRODUKTY (id_produktu, nazev, vyrobce, id_kategorie) VALUES (5, 'Pátý', 'vyrobce5', 3);
INSERT INTO PRODUKTY (id_produktu, nazev, vyrobce, id_kategorie) VALUES (6, 'Šestý', 'vyrobce6', 3);


-- ceny
INSERT INTO CENY (datum, id_produktu, cena) VALUES (CURRENT_TIMESTAMP, 1, 500);
INSERT INTO CENY (datum, id_produktu, cena) VALUES (CURRENT_TIMESTAMP, 2, 510);
INSERT INTO CENY (datum, id_produktu, cena) VALUES (CURRENT_TIMESTAMP, 3, 520);
INSERT INTO CENY (datum, id_produktu, cena) VALUES (CURRENT_TIMESTAMP, 4, 530);
INSERT INTO CENY (datum, id_produktu, cena) VALUES (CURRENT_TIMESTAMP, 5, 540);
INSERT INTO CENY (datum, id_produktu, cena) VALUES (CURRENT_TIMESTAMP, 6, 550);