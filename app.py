from flask import Flask, render_template, flash, request
from flask_login import current_user

from database import database
from database.database import get_db
from helpers.auth import is_admin_logged

app = Flask(__name__)
app.config.from_object('config')
database.init_app(app)

from views.auth import login_manager
login_manager.init_app(app)
from views import auth, categories, administration, products, items

app.register_blueprint(auth.bp)
app.register_blueprint(categories.bp)
app.register_blueprint(administration.bp)
app.register_blueprint(products.bp)
app.register_blueprint(items.bp)


@app.route('/')
def intro():
    after_login = request.args.get('after_login')
    after_logout = request.args.get('after_logout')
    db = get_db()
    kategorie = []
    try:
        kategorie = db.execute("SELECT * FROM kategorie").fetchall()
    except db.Error as e:
        flash('Nelze načíst data z databáze.', 'error')
        print(e)
    return render_template('intro.html', categories=kategorie, after_login=after_login, after_logout=after_logout,
                           is_admin_logged=is_admin_logged(), is_authenticated=current_user.is_authenticated)

if __name__ == '__main__':
    app.run()
