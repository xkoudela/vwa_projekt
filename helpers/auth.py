import hashlib

from flask_login import current_user

from database.database import get_db


def password_hash(password):
    # Create a hash of the entered password and comapre this hash with the hash in the database.
    salt = "moje_sul".encode('utf-8')  # A random string
    return hashlib.pbkdf2_hmac( # vrátí hash hesla na vstupu
        'sha256',  # An up-to-date hash digest algorithm for HMAC (old ones are already hacked)
        password.encode('utf-8'),  # Convert the password to bytes
        salt,  # Provide the salt
        100000  # It is recommended to use at least 100,000 iterations of SHA-256
    ).hex()

def is_admin(user_id):
    db = get_db()
    try:
        aktualni_role = db.execute(
            "SELECT nazev FROM uzivatele JOIN role_uzivatelu USING(id_uzivatele) JOIN role USING(id_role) "
            "WHERE id_uzivatele = ?",
            (user_id, )
        ).fetchone()[0]
        if (aktualni_role == "administrator"):
            return True
        else:
            return False
    except:
        return False

    # použití pro aktuálně přihlášeného uživatele: is_admin(current_user.get_id())

def is_admin_logged():
    return is_admin(current_user.get_id())
