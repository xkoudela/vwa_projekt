class User:
    def __init__(self, user_id: int, username: str):
        self.is_authenticated = True  # is this user logged in?

        self.user_id = str(user_id).encode("utf-8").decode("utf-8")  # must have id as unicode, not a number
        self.is_active = True
        # custom attributes
        self.username = username

    def get_id(self):
        return self.user_id
